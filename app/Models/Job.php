<?php

namespace App\Models;

use Illuminate\Support\Arr;

class Job
{
    public static function all(): array
    {
        return [

            [
                'id' => 1,
                'title' => 'Author',
                'salary' => '$40.000'
            ],
            [
                'id' => 2,
                'title' => 'Taxi driver',
                'salary' => '$35.000'
            ],
            [
                'id' => 3,
                'title' => 'Teacher',
                'salary' => '$38.000'
            ]
        ];
    }

    public static function find($id): array
    {
        $job = Arr::first(static::all(), fn($job) => $job['id'] == $id);

        if (!$job) {
            abort(404);
        }

        return $job;
    }
}
